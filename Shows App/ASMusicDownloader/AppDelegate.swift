//
//  AppDelegate.swift
//  ASMusicDownloader
//
//  Created by Adnan Siddiq on 11/20/14.
//  Copyright (c) 2014 Adnan Siddiq. All rights reserved.
//

import UIKit

let iOS7 = floor(NSFoundationVersionNumber) <= floor(NSFoundationVersionNumber_iOS_7_1)
let iOS8 = floor(NSFoundationVersionNumber) > floor(NSFoundationVersionNumber_iOS_7_1)


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        UITabBar.appearance().tintColor = UIColor.redColor()//UIColor(red: 0/255.0, green: 122.0/255, blue: 255.0/255, alpha: 1.0)
        
        UIApplication.sharedApplication().beginReceivingRemoteControlEvents()
        
        return true
    }

    override func canBecomeFirstResponder() -> Bool {
        return true
    }

    override func remoteControlReceivedWithEvent(event:UIEvent) {
        switch (event.subtype) {
        case UIEventSubtype.RemoteControlPlay:
            ASAudioPlayerManager.sharedInstance.play()
            break;
        case UIEventSubtype.RemoteControlPause:
            ASAudioPlayerManager.sharedInstance.pause()
            break;
        case UIEventSubtype.RemoteControlNextTrack:
            println("Next Track")
            if let media = ASAudioPlayerManager.sharedInstance.currentMedia {
                if let temp = ASHistoryManager.sharedInstance.mediaItemAfter(media) {
                    ASAudioPlayerManager.sharedInstance.pause()
                    ASAudioPlayerManager.sharedInstance.playMedia(temp);
                }
            }
            break;
        case UIEventSubtype.RemoteControlPreviousTrack:
            if let media = ASAudioPlayerManager.sharedInstance.currentMedia {
                if let temp = ASHistoryManager.sharedInstance.mediaItemBefore(media) {
                    ASAudioPlayerManager.sharedInstance.pause()
                    ASAudioPlayerManager.sharedInstance.playMedia(temp);
                }
            }

            break
        default:
            break;
        }
    }

}



