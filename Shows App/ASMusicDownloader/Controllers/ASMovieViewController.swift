//
//  ASMovieViewController.swift
//  ASMusicDownloader
//
//  Created by Adnan Siddiq on 12/10/14.
//  Copyright (c) 2014 Adnan Siddiq. All rights reserved.
//

import UIKit
import MediaPlayer

class ASMovieViewController: ASBaseViewController {

    @IBOutlet weak var feedListView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    internal var videoResult:NSArray!
    internal var refreshControl:UIRefreshControl = UIRefreshControl()
    
    @IBOutlet weak var infoView:UIView!
    @IBOutlet weak var infoLabel:UILabel!
    @IBOutlet weak var retryButton:UIButton!
    
    var selectedMedia:ASMedia!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        refreshControl.addTarget(self, action: Selector("refreshFeed"), forControlEvents: UIControlEvents.ValueChanged)
        self.feedListView.estimatedRowHeight = 100
        feedListView.addSubview(refreshControl)
        
        self.videoResult = NSArray()
        
        self.loadVideoFeed(nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "downloadComplete:", name: DownloadCompleteNotification, object: nil)
        
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        self.feedListView.reloadData()
    }
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: DownloadCompleteNotification, object: nil)
    }
    
    func downloadComplete(notification:NSNotification) {
        self.feedListView.reloadData()
    }
    
    func refreshFeed() {
        var text = searchBar.text.terminatingWitheSpaceAndNewLine()
        if text.length == 0 {
            refreshVideoFeed(nil)
        } else {
            refreshVideoFeed(text)
        }
    }
    
    // MARK: - Video Searh API
    func loadVideoFeed(query:String?) {
        self.showProgressView(self.navigationController?.view, message: "Loading...")
        self.refreshVideoFeed(query)
    }
    
    func refreshVideoFeed(query:String?) {
        ASVideoManager.sharedInstance.loadMovies(query, finishBlock: {(feeds:NSArray?, error:NSError?) -> () in
            if let e = error {
                if e.code == -1011 {
                    self.infoLabel.text = "Search your favorite songs by tapping the search field at the top."
                    self.retryButton.hidden = true
                } else {
                    self.infoLabel.text = e.localizedDescription
                    self.retryButton.hidden = false
                }
                self.infoView.hidden = false
            } else {
                self.videoResult = feeds!
                self.feedListView.reloadData()
                if self.videoResult.count == 0 {
                    self.infoView.hidden = false
                    self.infoLabel.text = "No reasult found."
                    self.retryButton.hidden = true
                } else {
                    self.infoView.hidden = true
                }
            }
            self.hideProgress()
            self.refreshControl.endRefreshing()
            
        })
    }
    
    func configureCell(feedCell:ASRSSFeedCell, indexPath:NSIndexPath) {
        var temp = videoResult[indexPath.row] as ASMedia
        
        feedCell.feedTitle.text = temp.mediaTitle
        feedCell.feedDate.text = temp.userName
        feedCell.totalViews.text = "Views: \(temp.totalViews)"
        
        feedCell.avatar.cancelLoading()
        if let url = temp.avatarLink {
            feedCell.avatar.image = nil
            feedCell.avatar.imageURL = url
        } else {
            feedCell.avatar.image = UIImage(named: "browse_peer_video_icon_selector_on")
        }
        
        if ASDownloadManager.sharedInstance.isItemAlreadyDownloding(temp) {
            
            feedCell.downloadLabel.text = "Downloading..."
        } else if (ASMyFilesManager.sharedInstance.isItemAlreadyDownload(temp) != nil) {
            feedCell.downloadLabel.text = "Downloaded"
        } else {
            feedCell.downloadLabel.text = String.timeFormatted(temp.mediaDuration)
        }
    }
    
    func playMedia(media:ASMedia) {
        
        ASHistoryManager.sharedInstance.saveMediaItems(media)
        var mediaURL = media.getStreamURL()
        var player = MPMoviePlayerViewController(contentURL: mediaURL)
        self.presentMoviePlayerViewControllerAnimated(player)
    }
    
    // MARK: - Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.destinationViewController.isKindOfClass(ASAudioPlayerViewController) {
            var vc = segue.destinationViewController as ASAudioPlayerViewController
            vc.media = sender as ASMedia
        }
    }
    
    func byteFormatter() -> NSByteCountFormatter {
        var byteFormatter = NSByteCountFormatter()
        byteFormatter.zeroPadsFractionDigits = true
        byteFormatter.adaptive = false
        byteFormatter.countStyle =  NSByteCountFormatterCountStyle.File
        return byteFormatter
    }
}

// MARK: - Selection Action

extension ASMovieViewController {
    
    @IBAction func searchTypeAction(sender: AnyObject) {
        if searchBar.text.terminatingWitheSpaceAndNewLine().length == 0 {
            loadVideoFeed(nil)
        } else {
            loadVideoFeed(searchBar.text.terminatingWitheSpaceAndNewLine())
        }
    }
}


extension ASMovieViewController:UITableViewDelegate, UITableViewDataSource {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.videoResult.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var feedCell = tableView.dequeueReusableCellWithIdentifier("cell") as ASRSSFeedCell
        
        configureCell(feedCell, indexPath: indexPath)
        return feedCell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return 240
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        selectedMedia = videoResult[indexPath.row] as ASMedia
        
        var actionAlert:UIActionSheet!
        
        if (ASDownloadManager.sharedInstance.isItemAlreadyDownloding(selectedMedia)) {
            actionAlert = UIActionSheet(title: selectedMedia.mediaTitle, delegate: self, cancelButtonTitle: nil, destructiveButtonTitle: nil, otherButtonTitles: "Play", "Cancel")
            actionAlert.cancelButtonIndex = 1
            
        } else if ((ASMyFilesManager.sharedInstance.isItemAlreadyDownload(selectedMedia)) != nil) {
            actionAlert = UIActionSheet(title: selectedMedia.mediaTitle, delegate: self, cancelButtonTitle: nil, destructiveButtonTitle: nil, otherButtonTitles: "Play", "Cancel")
            actionAlert.cancelButtonIndex = 1
        } else {
            actionAlert = UIActionSheet(title: selectedMedia.mediaTitle, delegate: self, cancelButtonTitle: nil, destructiveButtonTitle: nil, otherButtonTitles: "Play", "Download", "Cancel")
            actionAlert.cancelButtonIndex = 2
        }
        
        if actionAlert != nil {
            
            actionAlert.showFromTabBar(self.tabBarController?.tabBar)
        }
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
}

extension ASMovieViewController:UIActionSheetDelegate {
    
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int) {
        println("Action Sheet: \(buttonIndex)")
        
        if actionSheet.buttonTitleAtIndex(buttonIndex) == "Play" {
            playMedia(selectedMedia)
        } else if actionSheet.buttonTitleAtIndex(buttonIndex) == "Download" {
            ASDownloadManager.sharedInstance.downloadVideoItem(selectedMedia)
            
            if let tabvc = self.tabBarController {
                var temp = tabvc as MyTabbarController
                temp.setBadgeValue()
            }
            self.feedListView.reloadData()
        }
    }
    func actionSheetCancel(actionSheet: UIActionSheet) {
        println("Action Cancel")
    }
    
}

extension ASMovieViewController:UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarTextDidEndEditing(searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.resignFirstResponder()
        searchBar.text = ""
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.resignFirstResponder()
        
        var text = searchBar.text.terminatingWitheSpaceAndNewLine()
        if text.length == 0 {
            loadVideoFeed(nil)
        } else {
            loadVideoFeed(text)
        }
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        
    }
}



