//
//  ASPlayListViewController.swift
//  ASMusicDownloader
//
//  Created by Adnan Siddiq on 11/30/14.
//  Copyright (c) 2014 Adnan Siddiq. All rights reserved.
//

import UIKit
import MediaPlayer

class ASPlayListViewController: ASBaseViewController {

    @IBOutlet weak var fileList: UITableView!
    
    @IBOutlet weak var infoView:UIView!
    
    internal var downloads:NSMutableArray!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Recent Played"
        
        downloads = ASHistoryManager.sharedInstance.recentItems;
        self.infoView.hidden = downloads.count != 0
        self.fileList.tableFooterView = UIView(frame: CGRectZero)
    }
    

    func playMedia(media:ASMedia) {
        
        var savePath = String.documentDirctoryWithPath(media.mediaID + ".mp4")
        var mediaURL = NSURL(fileURLWithPath: savePath)!
        var player = MPMoviePlayerViewController(contentURL: mediaURL)
        self.presentMoviePlayerViewControllerAnimated(player)
    }
    
    @IBAction func playAllItems() {
        self.navigationController?.popViewControllerAnimated(true)
    }

}

extension ASPlayListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.downloads.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var feedCell = tableView.dequeueReusableCellWithIdentifier("cell") as ASRSSFeedCell
        
        var media = downloads[indexPath.row] as ASMedia
        
        feedCell.feedTitle.text = media.mediaTitle
        feedCell.feedDate.text = media.userName
        feedCell.totalViews.text = "Views: \(media.totalViews)"
        feedCell.downloadLabel.text = String.timeFormatted(media.mediaDuration)
        
        feedCell.avatar.cancelLoading()
        if let url = media.avatarLink {
            feedCell.avatar.image = nil
            feedCell.avatar.imageURL = url
        } else {
            feedCell.avatar.image = UIImage(named: "browse_peer_video_icon_selector_on")
        }
        
        return feedCell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var selectedMedia = downloads[indexPath.row] as ASMedia
        playMedia(selectedMedia);
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
}

