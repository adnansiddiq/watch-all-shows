//
//  ASDownloadManager.swift
//  ASMusicDownloader
//
//  Created by Adnan Siddiq on 11/21/14.
//  Copyright (c) 2014 Adnan Siddiq. All rights reserved.
//

import Foundation

class ASDownloadManager: NSObject {

    var downloadedItem:Array<ASMedia>!
    
    override init() {
        downloadedItem = Array<ASMedia>()
    }
    
    class var sharedInstance: ASDownloadManager {
        struct Static {
            static var instance: ASDownloadManager?
            static var token: dispatch_once_t = 0
        }
        
        dispatch_once(&Static.token) {
            Static.instance = ASDownloadManager()
        }
        
        return Static.instance!
    }

    func downloadVideoItem(media:ASMedia) {
        
        downloadedItem.append(media)
        media.downloadMedia()
    }
    
    func removeItem(media:ASMedia) {
        
        downloadedItem.removeObject(media)
    }
    
    func isItemAlreadyDownloding(media:ASMedia) -> Bool {
        
        var object = filter(downloadedItem) {(e:ASMedia) in
            e.mediaID == media.mediaID
        }
        return (object.count != 0)
    }

}

let SaveFileKey = "save_file_key"

class ASMyFilesManager: NSObject {
    
    var myFileItems:NSMutableArray!
    internal var needToDelete = Array<ASMedia>()
    
    var timer:NSTimer!
    
    override init() {
        super.init()
        
        if let cacheData = ASMyFilesManager.getSaveList() {
            myFileItems = cacheData
        } else {
            myFileItems = NSMutableArray()
        }
        NSTimer.scheduledTimerWithTimeInterval(10, target: self, selector: "needToDeleteMedia", userInfo: nil, repeats: true)
    }
    
    class var sharedInstance: ASMyFilesManager {
        struct Static {
            static var instance: ASMyFilesManager?
            static var token: dispatch_once_t = 0
        }
        
        dispatch_once(&Static.token) {
            Static.instance = ASMyFilesManager()
        }
        
        return Static.instance!
    }
    
    func saveMediaItems(media:ASMedia) {
        myFileItems.insertObject(media, atIndex: 0)
        ASMyFilesManager.saveList(myFileItems)
    }
    
    func removeItem(media:ASMedia) {
        if let temp = isItemAlreadyDownload(media) {
            var url = temp.getStreamURL()
            
            var fileManager = NSFileManager.defaultManager()
            if fileManager.removeItemAtURL(url, error: nil) {
                println("File Deleted \(index) - Count: \(myFileItems.count)")
            }
            
            myFileItems.removeObject(temp)
            ASMyFilesManager.saveList(myFileItems)
        }
    }
    
    func isItemAlreadyDownload(media:ASMedia) -> ASMedia? {
        
        if let predicate = NSPredicate(format: "mediaID = %@", media.mediaID) {
            var objects = myFileItems.filteredArrayUsingPredicate(predicate)
            return objects.last as? ASMedia
        }
        return nil
    }
    
    func getAudioItems() -> Array<ASMedia>? {
        
        if let predicate = NSPredicate(format: "mediaType = %d", MediaType.Audio.toValue()) {
            var objects = myFileItems.filteredArrayUsingPredicate(predicate)
            return objects as? Array<ASMedia>
        }
        
        return nil
    }
    func getVideoItems() -> Array<ASMedia>? {
        
        if let predicate = NSPredicate(format: "mediaType = %d", MediaType.Video.toValue()) {
            var objects = myFileItems.filteredArrayUsingPredicate(predicate)
            return objects as? Array<ASMedia>
        }
        
        return nil
    }

    
    class func saveList(list:AnyObject) {
        
        var data = NSKeyedArchiver.archivedDataWithRootObject(list)
        NSUserDefaults.saveObject(data, key: SaveFileKey)
    }
    
    class func getSaveList() -> NSMutableArray? {
    
        if let saveData = NSUserDefaults.getObject(SaveFileKey) as? NSData {
            var data = NSKeyedUnarchiver.unarchiveObjectWithData(saveData) as NSMutableArray
            return data
        } else {
            return nil
        }
    }
}
// MARK: - Syn Data
extension ASMyFilesManager {
    
    func synWithWeb() {
        
        println("Sync Files")
        
        for temp in myFileItems {
            
            var media = temp as ASMedia
            if media.mediaType == MediaType.Audio.toValue() {
                
                var urlstr = "https://api.soundcloud.com/tracks/" + media.mediaID + "1.json?client_id=" + clientID
                println("URL: \(urlstr)")
                var request = NSURLRequest(URL:NSURL(string: urlstr)!)
                
                var manger = AFHTTPRequestOperationManager()
                var serlizer = AFJSONResponseSerializer()
                serlizer.acceptableContentTypes = NSSet(object: "application/json")
                serlizer.stringEncoding = NSUTF8StringEncoding
                manger.responseSerializer = serlizer
                
                var feedOperation = manger.HTTPRequestOperationWithRequest(request, success: {(operation: AFHTTPRequestOperation!, responseObject: AnyObject!) -> Void in
                    
                        println("Found Item: \(media.mediaID)")
                    
                    }, failure:{ (operation: AFHTTPRequestOperation!, error:NSError!) -> Void in
                        
                        self.needToDelete.append(media)
                        println("Error: \(error.localizedDescription) response: \(operation.responseString)")
                })
                feedOperation.start()
            } else {
                
                var feedURL = "http://gdata.youtube.com/feeds/api/videos/" + media.mediaID
                var url = NSURL(string: feedURL.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!)
                var urlRequest = NSURLRequest(URL: url!)
                
                var manger = AFHTTPRequestOperationManager()
                var serlizer = AFXMLParserResponseSerializer()
                serlizer.acceptableContentTypes = NSSet(object: "application/atom+xml")
                serlizer.stringEncoding = NSUTF8StringEncoding
                manger.responseSerializer = serlizer
                
                var feedOperation = manger.HTTPRequestOperationWithRequest(urlRequest, success: {(operation: AFHTTPRequestOperation!, responseObject: AnyObject!) -> Void in
                    
                    if !responseObject.isKindOfClass(NSXMLParser) {
                        self.needToDelete.append(media)
                        
                    }
                    
                    }, failure:{ (operation: AFHTTPRequestOperation!, error:NSError!) -> Void in
                        
                        self.needToDelete.append(media)
                })
                feedOperation.start()

            }

            
        }
        if timer != nil {
            if timer.valid {
                timer.invalidate()
            }
        }
        timer = NSTimer.scheduledTimerWithTimeInterval(60*60, target: self, selector: "synWithWeb", userInfo: nil, repeats: false)
    }
    
    func needToDeleteMedia() {
        
        println("Delete Items Count: \(needToDelete.count)")
        for media in needToDelete {
            removeItem(media)
            ASHistoryManager.sharedInstance.removeItem(media)
        }
        
        needToDelete.removeAll(keepCapacity: false)
    }
}


