//
//  ASFeedManager.swift
//  Bilancio Sociale Provincia Cosenza
//
//  Created by Adnan Siddiq on 11/2/14.
//  Copyright (c) 2014 Adnan Siddiq. All rights reserved.
//

import Foundation

let clientID = "717ce0a093aae061dbdabbad225fa30e"

typealias CompletionBlock = (feeds:NSArray?, error:NSError?) -> ()

class SearchResult:NSObject {
    var searchString:NSString!
    var searchIndex:Int!
    var searchType:SearchType!
    
    init(content:NSString!, index:Int!, type:SearchType!) {
        
        searchString = content
        searchIndex = index
        searchType = type
    }
}

class ASFeedManager: NSObject {
    
    internal var itemInfo:NSMutableDictionary?
    internal var currentKey:NSString?
    internal var responseBlock:CompletionBlock?

    internal var feeds:NSMutableArray?
    
    class var sharedInstance: ASFeedManager {
        struct Static {
            static var instance: ASFeedManager?
            static var token: dispatch_once_t = 0
        }
        
        dispatch_once(&Static.token) {
            Static.instance = ASFeedManager()
        }
        
        return Static.instance!
    }
    
    func loadFeed(query:NSString?, finishBlock:CompletionBlock?) {
        
        responseBlock = finishBlock
        var feedURL:String
        if let q = query {
            feedURL = "https://api.soundcloud.com/tracks.json?client_id=" + clientID + "&q=" + q + "&limit=20"
        } else {
            feedURL = "http://wv.smarnovative.com/YouTube-Downloader-master/latestsounds.php"
        }
        
        var url = NSURL(string: feedURL.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!)
        var urlRequest = NSURLRequest(URL: url!)
        
        var manger = AFHTTPRequestOperationManager()
        var serlizer = AFJSONResponseSerializer()
        serlizer.acceptableContentTypes = NSSet(object: "application/json")
        serlizer.stringEncoding = NSUTF8StringEncoding
        manger.responseSerializer = serlizer
        
        var feedOperation = manger.HTTPRequestOperationWithRequest(urlRequest, success: {(operation: AFHTTPRequestOperation!, responseObject: AnyObject!) -> Void in
            
            var responseArray = responseObject as NSArray
            
            self.feeds = NSMutableArray()
            
            for info in responseArray {
                
                var media = ASMedia(info: info as NSDictionary, type: MediaType.Movies)
                
                self.feeds?.addObject(media)
            }
            
            finishBlock!(feeds: self.feeds, error: nil)
            
        }, failure:{ (operation: AFHTTPRequestOperation!, error:NSError!) -> Void in
            
            finishBlock!(feeds: nil, error: error)
        })
        feedOperation.start()
        
    }
}

