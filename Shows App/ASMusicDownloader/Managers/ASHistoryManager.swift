//
//  ASHistoryManager.swift
//  ASMusicDownloader
//
//  Created by Adnan Siddiq on 11/30/14.
//  Copyright (c) 2014 Adnan Siddiq. All rights reserved.
//

import UIKit

let SaveHistoryKey = "save_history_key"

class ASHistoryManager: NSObject {
   
    var recentItems:NSMutableArray!
    var deleteItem = NSMutableArray()
    
    override init() {
        super.init()
        if let cacheData = ASHistoryManager.getSaveList() {
            recentItems = cacheData
            
            println("\(NSStringFromClass(recentItems.classForCoder))")
        } else {
            recentItems = NSMutableArray()
        }
    }
    
    class var sharedInstance: ASHistoryManager {
        struct Static {
            static var instance: ASHistoryManager?
            static var token: dispatch_once_t = 0
        }
        
        dispatch_once(&Static.token) {
            Static.instance = ASHistoryManager()
        }
        
        return Static.instance!
    }
    
    func saveMediaItems(media:ASMedia) {
        
        if let temp = isItemAlreadyExsit(media) {
            deleteItem.addObject(temp) // Fucking Idea to save crash
            recentItems.replaceObjectAtIndex(recentItems.indexOfObject(temp), withObject: media)
        } else {
            recentItems.insertObject(media, atIndex: 0)
        }
        ASHistoryManager.saveList(recentItems)
    }
    
    func removeItem(media:ASMedia) {
        if let temp = isItemAlreadyExsit(media)  {
            println("Delete From Recent")
            deleteItem.addObject(temp)
            recentItems.removeObject(temp)
            ASHistoryManager.saveList(recentItems)
        }
    }
    
    func isItemAlreadyExsit(media:ASMedia) -> ASMedia? {
        
        if let predicate = NSPredicate(format: "mediaID = %@", media.mediaID) {
            var objects = recentItems.filteredArrayUsingPredicate(predicate)
            return objects.last as? ASMedia
        }
        return nil
    }
    
    func indexOfMedia(media:ASMedia) -> Int? {
        
//        var index = 1
//        for temp in recentItems {
//            
//            if media.mediaID == temp.mediaID {
//                return index
//            }
//            index += 1
//        }
        return recentItems.indexOfObject(media) + 1
    }
    
    func mediaItemAfter(media:ASMedia) -> ASMedia? {

        var findItem = false
        var output:ASMedia!
        
        var index = recentItems.indexOfObject(media)
        if index == recentItems.count - 1 {
            output = recentItems.firstObject as ASMedia
        } else if index < recentItems.count {
            output = recentItems[index + 1] as ASMedia
        } else {
            output = nil
        }
        return output
    }
    
    func mediaItemBefore(media:ASMedia) -> ASMedia? {
        
        var findItem = false
        var output:ASMedia!
        
        var index = recentItems.indexOfObject(media)
        if index == 0 {
            output = recentItems.lastObject as ASMedia
        } else if index < recentItems.count {
            output = recentItems[index - 1] as ASMedia
        } else {
            output = nil
        }
        return output
    }
    
    func getMediaOfURL(url:NSURL) -> ASMedia? {
        if let urlString = url.absoluteString {
            var compnent = urlString.componentsSeparatedByString("?")
            var firstStr = compnent[0] as String
            compnent = firstStr.componentsSeparatedByString("/")
            
            var temp = compnent.last! as String
            var localTemp = temp.componentsSeparatedByString(".")
            var mediaID:String
            if localTemp.count == 2 {
                mediaID = localTemp[0]
            } else {
                mediaID = compnent[compnent.count - 2]
            }
            println("Media ID: \(mediaID)")
            
            if let predicate = NSPredicate(format: "mediaID = %@", mediaID) {
                var objects = recentItems.filteredArrayUsingPredicate(predicate)
                return objects.last as? ASMedia
            }
        }
        return nil
    }
    
    class func saveList(list:AnyObject) {
        
        var data = NSKeyedArchiver.archivedDataWithRootObject(list)
        NSUserDefaults.saveObject(data, key: SaveHistoryKey)
    }
    
    class func getSaveList() -> NSMutableArray? {
        
        if let saveData = NSUserDefaults.getObject(SaveHistoryKey) as? NSData {
            var data = NSKeyedUnarchiver.unarchiveObjectWithData(saveData) as NSMutableArray
            return data
        } else {
            return nil
        }
    }

}
