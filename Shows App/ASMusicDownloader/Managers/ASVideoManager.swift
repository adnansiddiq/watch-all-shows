//
//  ASVideoManager.swift
//  Bilancio Sociale Provincia Cosenza
//
//  Created by Adnan Siddiq on 11/14/14.
//  Copyright (c) 2014 Adnan Siddiq. All rights reserved.
//

import Foundation


class ASVideoManager: NSObject {
    
    internal var itemInfo:NSMutableDictionary?
    internal var currentKey:NSString?
    internal var videos:NSMutableArray?
    internal var mediaType:MediaType!
    
    internal var reqManger:AFHTTPRequestOperationManager!
    internal var reqOperation:AFHTTPRequestOperation!
    
    internal var inprogress:Bool = false

    
    class var sharedInstance: ASVideoManager {
        struct Static {
            static var instance: ASVideoManager?
            static var token: dispatch_once_t = 0
        }
        
        dispatch_once(&Static.token) {
            Static.instance = ASVideoManager()
        }
        
        return Static.instance!
    }
    
    override init() {
        super.init()
        reqManger = AFHTTPRequestOperationManager()
        var serlizer = AFXMLParserResponseSerializer()
        serlizer.acceptableContentTypes = NSSet(object: "application/atom+xml")
        serlizer.stringEncoding = NSUTF8StringEncoding
        reqManger.responseSerializer = serlizer
    }
    func loadMovies(query:NSString?, finishBlock:CompletionBlock?) {
        
        var feedURL:String
        if let q = query {
            feedURL = "https://gdata.youtube.com/feeds/api/videos?category=Shows&q=" + q
        } else {
            feedURL = "https://gdata.youtube.com/feeds/api/videos?category=Shows&q=latest"
//            feedURL = "https://gdata.youtube.com/feeds/api/charts/shows/most_popular?v=2"
        }
        mediaType = MediaType.Movies
        loadData(feedURL, finishBlock: finishBlock)
    }


    func loadTralier(query:NSString?, finishBlock:CompletionBlock?) {
        
        var feedURL:String
        if let q = query {
            feedURL = "https://gdata.youtube.com/feeds/api/videos?category=Trailer&q=" + q
        } else {
            feedURL = "https://gdata.youtube.com/feeds/api/videos?category=Trailer&q=latest"
        }
        mediaType = MediaType.Trailer
        loadData(feedURL, finishBlock: finishBlock)
    }
    
    func loadData(query:NSString!, finishBlock:CompletionBlock?) {
        
        var url = NSURL(string: query.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!)
        var urlRequest = NSURLRequest(URL: url!)
        
        if inprogress {
            reqOperation.cancel()
        }
        
        println("Request: \(query)")
        
        reqOperation = reqManger.HTTPRequestOperationWithRequest(urlRequest, success: {(operation: AFHTTPRequestOperation!, responseObject: AnyObject!) -> Void in
            
            self.inprogress = false
            
            if responseObject.isKindOfClass(NSXMLParser) {
                
                println("Success")
                
                NSUserDefaults.saveObject(operation.responseData, key: MyConstants.kVideoCacheResponse)
                
                self.parseCacheResponse(responseObject as NSXMLParser)
                
                if finishBlock != nil {
                    
                    finishBlock!(feeds: self.videos, error: nil)
                }
            }
            
            }, failure:{ (operation: AFHTTPRequestOperation!, error:NSError!) -> Void in
                println("Error")
                if finishBlock != nil && !operation.cancelled {
                    finishBlock!(feeds: nil, error: error)
                }
                self.inprogress = false
        })
        reqOperation.start()
        inprogress = true
    }
    
    func parseCacheResponse(xmlParser:NSXMLParser) {
        
        self.videos = NSMutableArray()
        xmlParser.delegate = self
        xmlParser.parse()
    }
    
    func getLocaleID() -> String {
        
        var identifair = NSLocale.currentLocale().localeIdentifier
        var compnent = identifair.componentsSeparatedByString("_")
        if compnent.count == 2 {
            return compnent.last!
        } else {
            return "US"
        }
    }
}

// MARK: - XMLParser

extension ASVideoManager: NSXMLParserDelegate {
    
    func parser(parser: NSXMLParser!, didStartElement elementName: String!, namespaceURI: String!, qualifiedName qName: String!, attributes attributeDict: [NSObject : AnyObject]!) {
        
        currentKey = nil
        if elementName == "entry" {
            itemInfo = NSMutableDictionary()
        }
        
        if itemInfo == nil {
            return
        }
        
        var dictInfo = itemInfo!
        
        if elementName == "title" || elementName == "published" || elementName == "content" || elementName == "id" || elementName == "name" {
            currentKey = elementName
        } else if elementName == "media:player" {
            dictInfo.setObject(attributeDict["url"]!, forKey: "video")
        } else if elementName == "media:thumbnail" {
            if attributeDict["height"]?.integerValue == 360 || attributeDict["height"]?.integerValue == 300 {
                if (dictInfo["picture"] == nil) {
                    dictInfo.setObject(attributeDict["url"]!, forKey: "picture")
                }
            }
        } else if elementName == "yt:duration" {
            dictInfo["duration"] = attributeDict["seconds"]!
        } else if elementName == "yt:statistics" {
            dictInfo["favoriteCount"] = attributeDict["favoriteCount"]!
            dictInfo["viewCount"] = attributeDict["viewCount"]
        }
    }
    
    func parser(parser: NSXMLParser!, foundCharacters string: String!) {
        
        if (currentKey != nil) {
            if (string.terminatingWitheSpaceAndNewLine().length != 0) {
                var dictInfo = itemInfo!
                dictInfo[currentKey!] = string.terminatingWitheSpaceAndNewLine()
            }
        }
    }
    
    
    func parser(parser: NSXMLParser!, didEndElement elementName: String!, namespaceURI: String!, qualifiedName qName: String!) {
        
        if elementName == "entry" {
            
            var feedObj = ASMedia(info: self.itemInfo!, type:mediaType)
            self.videos?.addObject(feedObj)
            itemInfo = nil
        }
    }
}
